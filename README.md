# OpenTT: LARC API

LARC is the "list of authorized racket coverings" for table tennis.

LARC API provides means to store the data and provide it via a REST API.
This is **not** an official app of ITTF or DTTB or such.

- feature requests, problems etc.: <https://gitlab.com/opentt/larc-api/-/issues>
- changes: [changelog](changelog.md)


## Open-TT

LARC is part of the project "OpenTT" which provides open documents and applications for table tennis:

- <https://opentt.de/>
- <https://gitlab.com/opentt>


## Git-Repository

The branching model regards to the stable mainline model described in <http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git>

This means, there is always a stable mainline, the `main` branch.
This branch ist always compileable and testable, both without errors.

## Legal stuff

License of the app: GNU General Public License.
See file [COPYING](COPYING).

Which means the app is free and open source

- you can use the program as you want, even commercially
- you can share the program as you like
- if you change the source code, you have to distribute it under the same license, and you have to provide the source code of the programs


### Copyright

Copyright 2019-2021 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License.

See [COPYING](COPYING) for details.

This file is part of OpenTT: LARC API.

OpenTT: LARC API is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenTT: LARC API is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenTT: LARC API. If not, see <http://www.gnu.org/licenses/>.
